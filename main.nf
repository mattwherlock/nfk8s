#!/usr/bin/env nextflow

Channel.fromFilePairs("${params.fastqDir}*-*_L00*_R{1,2}_*.fastq.gz").set{fastqPairs}

refFasta = Channel.value(file(params.refFasta))
refFastaIndex = Channel.value(file(params.refFastaIndex))
refDict = Channel.value(file(params.refDict))
bedFile = Channel.value(file(params.bedFile))
bwaPathLength = params.bwaIndexFiles.lastIndexOf(File.separator)
bwaDir = params.bwaIndexFiles.substring(0,bwaPathLength+1)
bwaBase = params.bwaIndexFiles.substring(bwaPathLength+1)
bwaIndex = Channel.fromPath(bwaDir)


process bwaMem {
    cpus 4
    memory "8GB"
    
    publishDir path: "${params.outputDir}/logs", pattern: "*.sh", mode: "copy", saveAs: {"bwaMem.${sampleName}.command.sh"}
    publishDir path: "${params.outputDir}/logs", pattern: "*.log", mode: "copy", saveAs: {"bwaMem.${sampleName}.command.log"}
    publishDir path: "${params.outputDir}/logs", pattern: "*.err", mode: "copy", saveAs: {"bwaMem.${sampleName}.command.err"}

    publishDir path: "${params.outputDir}/alignment", pattern: "*.cram*", mode: "copy"

    input:
    set val(pairName), file(readPair) from fastqPairs
    file bwaIndexFiles from bwaIndex.collect()
    file refFasta

    output:
    file ".command.{sh,err,log}"
    file "*.cram" into cram
    file "*.cram.crai" into cramIndex

    script:
    refFasta = "${bwaIndexFiles}/${bwaBase}"
    sampleName = pairName.split('_')[0]
    """
    bwa mem \
    -t ${params.bwaThreads} \
    $refFasta \
    -R '@RG\\tID:FC1.${readPair[0].baseName.split('_')[2]}\\tPL:ILLUMINA\\tSM:${sampleName}\\tLB:${sampleName}' \
    -M \
    ${readPair} | \
    samtools sort \
    --reference ${refFasta} \
    -O CRAM \
    -o ${sampleName}.cram \
    - \
    && \
    samtools index \
    ${sampleName}.cram
    """
}


process snpGenotypeCall {
    publishDir path: "${params.outputDir}/logs", pattern: "*.sh", mode: "copy", saveAs: {"snpGenotypeCall.${sampleName}.command.sh"}
    publishDir path: "${params.outputDir}/logs", pattern: "*.log", mode: "copy", saveAs: {"snpGenotypeCall.${sampleName}.command.log"}
    publishDir path: "${params.outputDir}/logs", pattern: "*.err", mode: "copy", saveAs: {"snpGenotypeCall.${sampleName}.command.err"}

    input:
    file cram
    file cramIndex
    file refFasta
    file refFastaIndex
    file refDict
    file bedFile

    output:
    file ".command.{sh,err,log}"
    file "*.snp_genotype.g.vcf.gz" into snpGenotypeGvcf
    file "*.snp_genotype.g.vcf.gz.tbi" into snpGenotypeGvcfIndex

    script:
    sampleName = cram.baseName
    """
    /gatk/gatk HaplotypeCaller \
    -R ${refFasta} \
    -I ${cram} \
    -O ${sampleName}.snp_genotype.g.vcf.gz \
    -A Coverage \
    -ERC BP_RESOLUTION \
    -L ${bedFile}
    """
}


process snpGenotypeGenotype {
    publishDir path: "${params.outputDir}/logs", pattern: "*.sh", mode: "copy", saveAs: {"snpGenotypeGenotype.${sampleName}.command.sh"}
    publishDir path: "${params.outputDir}/logs", pattern: "*.log", mode: "copy", saveAs: {"snpGenotypeGenotype.${sampleName}.command.log"}
    publishDir path: "${params.outputDir}/logs", pattern: "*.err", mode: "copy", saveAs: {"snpGenotypeGenotype.${sampleName}.command.err"}

    publishDir path: "${params.outputDir}/genotype", pattern: "*.snp_genotype.vcf*", mode: "copy"

    input:
    file snpGenotypeGvcf
    file snpGenotypeGvcfIndex
    file refFasta
    file refDict
    file refFastaIndex
    file bedFile

    output:
    file ".command.{sh,err,log}"
    file "*.snp_genotype.vcf*"

    script:
    sampleName = snpGenotypeGvcf.baseName.tokenize('.')[0]
    """
    /gatk/gatk GenotypeGVCFs \
    -R ${refFasta} \
    -V ${snpGenotypeGvcf} \
    -O ${sampleName}.snp_genotype.vcf \
    -L ${bedFile} \
    --include-non-variant-sites
    """
}
